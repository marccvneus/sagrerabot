# -*- coding: latin-1 -*-
import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineQueryResultArticle, InputTextMessageContent, InlineKeyboardMarkup, InlineKeyboardButton
import time
import urllib3
from pprint import pprint
import json
import unicodedata,re
import os
import sys

################### IMPORTANT!!!! ################################
# Part de codi per testejar el bot a PythonAnywhere. S'haurà de treure en la definitiva
import urllib3
proxy_url = "http://proxy.server:3128"
telepot.api._pools = {
    'default': urllib3.ProxyManager(proxy_url=proxy_url, num_pools=3, maxsize=10, retries=False, timeout=30),
}
telepot.api._onetime_pool_spec = (urllib3.ProxyManager, dict(proxy_url=proxy_url, num_pools=1, maxsize=1, retries=False, timeout=30))
################# Fi de la part per PythonAnywhere ##################


#dev POSAR AQUI SOTA DADES, TOKEN DEL BOT
#token = aquí el token de proves si n'hi ha
#BotXarxa
token = ''
TelegramBot = telepot.Bot(token)

#Identificador del xat on s'envien els resultats. RavalSuport
admin_chat_id= ie chat on s'han d'enviar les respostes
#Identificador canal de proves
#admin_chat_id= id del chat de proves si n'hi ha

el_form={}
msg_usr='MISSATGE: '

text_sortint=u"Envian's el contacte que vulguis fer servir i més observacions si ho creus necessari"

arr_sortida={}
arr_sortida['ajuda']=u"A quin carrer vius? Potser ja hi ha gent organitzat-se al teu carrer!\n\n\
Deixa'ns el teu nom, carrer, telèfon i email, i digue'ns què necessites... i mirarem d'organitzar-nos!\n\n\
ℹ️Recorda, soc un bot! El que escriguis aquí només ho llegirem tu i jo: no és públic."

arr_sortida['ofereixo']=u"Deixa'ns el teu nom, carrer, telèfon i email, i detalla què ofereixes... i mirarem d'organitzar-nos!\n\n\
ℹ️Recorda, soc un bot! El que escriguis aquí només ho llegirem tu i jo: no és públic."

arr_sortida['laboral']=u"T'han fet fora de la feina? Volen fer un ERO/ERTO a la teva empresa? Estan vulnerant els teus drets laborals?\n\n\
Explica'ns el teu problema i deixa'ns el teu nom, carrer, telèfon i email. \n\n\
ℹ️Recorda, soc un bot! El que escriguis aquí només ho llegirem tu i jo: no és públic."

arr_sortida['habitatge']=u"T'han pujat el lloguer i no el pots pagar? Et volen fer fora de casa? Tens risc de desnonament?\n\n\
Explica'ns el teu problema i deixa'ns el teu nom, carrer, telèfon i email. Des del Sindicat d'Habitatge de La Sagrera t'oferiran l'assessorament que necessites!\n\n\
ℹ️Recorda, soc un bot! El que escriguis aquí només ho llegirem tu i jo: no és públic."


with open('estructura.json', 'r') as json_file: #obre el json i el guarda com a estructura
    estructura = json.load(json_file)

#json amb elements actuals.
sub_estructura=estructura
envia_msg=0

def handle(msg):
    #Funció que rep els textos escrits al xat
    global el_form
    global sub_estructura, envia_msg, msg_usr,text_sortint

    pprint(msg)
    content_type, chat_type, chat_id = telepot.glance(msg)
    comanda=msg['text']
    try:
        usr_id=msg['from']['id']
    except:
        usr_id=chat_id


    if  comanda == '/suport':
        envia_msg=0
        msg_usr='MISSATGE: '
        text_sortint=''
        el_form={}

        inline_array = []
        sub_estructura=estructura['questionari']['opcions']
        for unelement in estructura['questionari']['opcions'].values():

            eltext=unelement['text']
            ID=unelement['ID']

            inline_array.append(InlineKeyboardButton(text=eltext,callback_data=ID))

        keyboard_elements = [[element] for element in inline_array]

        el_form=TelegramBot.sendMessage(usr_id, estructura['questionari']['titol'],reply_markup=InlineKeyboardMarkup(inline_keyboard=keyboard_elements))

    elif comanda == '/hola':
        TelegramBot.sendMessage(usr_id,PrintInfo())
    else:
        if envia_msg == 2:
            missatge_final=msg['text']
            tel=0
            mail=0
            #Es comprova si hi ha un telefon
            if re.search("(^| )[0-9.() -]{5,}( |$)", missatge_final):
                tel=1
            if re.search('[\w\.-]+@[\w\.-]+\.\w+', missatge_final):
                mail=1
            if tel==1 or mail ==1:
                #S'incorpora el contingut del missatge per adjuntar al formulari
                msg_usr+=", "+missatge_final+", "+msg['from']['first_name']
                envia_msg=0
                TelegramBot.sendMessage(admin_chat_id, msg_usr)
                TelegramBot.sendMessage(usr_id, "Hem rebut el teu missatge! ens posarem en contacte amb tu. Salut!")
                msg_usr='MISSATGE: '
            else:
                missatge_tel_mail=u"el teu missatge sembla que no conté una forma de contacte, correu electrònic o telèfon. Siusplau fes-nos arribar una forma de contacte. Esperem..."
                plainstring1 = missatge_tel_mail.encode('latin-1')
                TelegramBot.sendMessage(usr_id, plainstring1)
        else:
            envia_msg=0
            msg_usr='MISSATGE: '
            text_sortint=''
            el_form={}            
            TelegramBot.sendMessage(usr_id,PrintBenvinguda())



def UsrOptions(msg):
    #Esborrat de l'anterior formulari.
    global el_form
    global sub_estructura,envia_msg,msg_usr,text_sortint
    
    elmissatge = telepot.message_identifier(el_form)
    TelegramBot.deleteMessage(elmissatge)    
    #Funció que rep els clicks del formulari
    seleccio=msg['data']
    usr_id=msg['from']['id'] 

    #Carrega menu
    inline_array=[]

    #Es comprova que no som al final del formulari
    elements=0

    msg_usr+=" ,"+sub_estructura[seleccio]['text']
    for unelement in sub_estructura[seleccio].values():
        try:
            elements+=1
            eltext=unelement['text']
            ID=unelement['ID']
            inline_array.append(InlineKeyboardButton(text=eltext,callback_data=ID))
        except:
            elements-=1

    keyboard_elements = [[element] for element in inline_array]

    if seleccio == "missatge":
        envia_msg=2
        text_sortint=u"    Escriu al xat allò que ens vols fer arribar! Deixa'ns el teu nom, carrer, telèfon i email si necessites que algú es posi en contacte amb tu.\n\n\
ℹ️Recorda, soc un bot! El que escriguis aquí només ho llegirem tu i jo: no és públic."
        plainstring1 = text_sortint.encode('latin-1')
        TelegramBot.sendMessage(usr_id,plainstring1)
    elif elements==0:
        #Es demana text
        if re.match("^a", seleccio):
            text_sortint=arr_sortida['ajuda']
        if re.match("^s", seleccio):
            text_sortint=arr_sortida['ofereixo']
        if re.match("habitatge", seleccio):
            text_sortint=arr_sortida['habitatge']
        if re.match("laboral", seleccio):
            text_sortint=arr_sortida['laboral']                                    
        envia_msg=2
        #missatge=u"Envian's el contacte que vulguis fer servir i més observacions si ho creus necessari"

        plainstring1 = text_sortint.encode('latin-1')
        TelegramBot.sendMessage(usr_id,plainstring1)
    else:
        el_form=TelegramBot.sendMessage(usr_id, "Digue'ns!",reply_markup=InlineKeyboardMarkup(inline_keyboard=keyboard_elements))

    sub_estructura=sub_estructura[seleccio]

    return 0


def PrintBenvinguda():

    msg=u"Hola!\n\
Soc el bot de la Xarxa de Suport Mutu del Sindicat d'Habitatge de la Sagrera @suportsagrera\n\n\
Vius a La Sagrera i...\n\n\
▶️...vols ajudar els teus veïns i veïnes?\n\
▶️...necessites ajuda d'algun tipus?\n\
▶️...tens alguna consulta perquè t'han fet fora de la feina o \
    estan vulnerant els teus drets?\n\
▶️...t'han pujat el lloguer i no el pots pagar, o et volen fer fora de casa?\n\n\
👉Clica aquí /hola o escriu /hola al xat!\n\
ℹ️Recorda, soc un bot! El que escriguis aquí només ho llegirem tu i jo: no és públic.\n"
    msg = msg.encode('latin-1')
    return msg

def PrintInfo() :

    msg=u"\nHoli! Per OFERIR el teu suport o disponibilitat, o bé si tens alguna NECESSITAT, o si ens vols fer arribar alguna INFORMACIÓ, clica aquí /suport i segueix les instruccions."

    return msg

def PrintSupBCN() :

    msg="\nMapa dels diferents grups de suport de la ciutat\
        https://t.co/rDcL1l35vY?amp=1\
        "
    msg = msg.encode('latin-1')
    return msg

def PrintContactes() :

    msg = "061 - CatSalut respon\n\
    suportsagrera@gmail.com - Correu electrònic de la xarxa de suport mutu de La Sagrera\n\
    https://twitter.com/SagreraSindicat - Compte de twitter del Sindicat d'Habitatge de La Sagrera"
    return msg


MessageLoop(TelegramBot,{'chat': handle, 'callback_query': UsrOptions}).run_as_thread()
print ('A tope ...')
#TelegramBot.message_loop(handle).run_as_thread()



# Keep the program running.
while 1:
    time.sleep(10)